#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Eskimon'
SITENAME = 'Cool Cookies'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('Facebook', '#'),
          ("Twitter", "#"),
          ("Google+", "#"),
          ("ZdS", "#"),)

DEFAULT_PAGINATION = 5

STATIC_PATHS = ['images']

THEME = 'mon-theme'
THEME_STATIC_DIR = 'static'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
