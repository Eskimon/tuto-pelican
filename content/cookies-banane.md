Title: Cookies à la banane
Date: 2018-05-04 13:42
Tags: cookie, banane
Category: Cookie
Authors: Eskimon
Summary: Recette de cookies à la banane

Partons à l'aventure en faisant des cookies avec de la banane et du chocolat

# Ingrédients

*(Pour une vingtaine de cookies)*

- 170 g de bananes (à peser sans la peau ;) )
- 150 g de farine
- 60 g de beurre
- 5 g de levure chimique
- 1 pincée de sel
- 1 oeuf
- 100 g de sucre en poudre
- 1 cuillère à café de vanille liquide
- 130 g de chocolat

# Étape 1

Préchauffez le four à 190°C (et pas plus!!).

# Étape 2

Mélangez la farine, la levure, le sel.

# Étape 3

Pelez les bananes et écrasez-les à la fourchettes.

# Étape 4

Battez le beurre (bien ramolli) avec le sucre ; ajoutez la banane écrasée, l'oeuf, la vanille. Mélangez bien. Ajoutez le mélange de farine.

# Étape 5

Ajoutez les pépites (+ noix). La pâte doit être molle mais pas trop liquide.

# Étape 6

Faites des petits tas à l'aide de 2 cuillères à café (de la taille d'une noix, pas plus!!) sur une plaque (soit beurrée soit recouverte de papier sulfurisé, + facile!).

# Étape 7

Faites cuire env 6/7 mn (selon votre four).

# Étape 8

Pour la 1ère fournée, surveillez bien, ça cuit très vite. Retirez les cookies dès qu'ils sont bruns à la base.
