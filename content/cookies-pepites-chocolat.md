Title: Cookies aux pépites de chocolat
Date: 2018-05-04 12:42
Tags: cookie, chocolat
Category: Cookie
Authors: Eskimon
Summary: Ma recette pour des supers cookies aux pépites de chocolat.

Une première recette pour débuter dans l'art du cookie.

# Ingrédients

*(Pour 10 cookies)*

- 100g de pepites de chocolat
- 1 cuiller à café de levure
- 1/2 c à café de sel
- 150g de farine
- 1 sachet de sucre vanillé
- 1 œuf
- 85g de sucre (peut descendre à 50g)
- 75g de beurre (peut descendre à 50g)

# Étape 1 : Mélange

Ramollir le beurre au micro-ondes (sans le faire fondre).

Mélanger beurre, œuf, sucre et sucre vanillé. Ajouter la farine, le sel et la levure petit à petit, puis les pépites de chocolat.

# Étape 2 : Cuisson

Faites de petites boules, les mettre sur du papier sulfurisé.

Enfournez à 180°C pendant 10 à 12 min (suivant la texture que vous désirez).

# Étape 3 : Dégustez !

:D

![Des cookies](/images/cookies.jpg)